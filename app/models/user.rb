class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlacklist

  # Validations to ensure the correct data is being entered
  validates :first_name, presence: true
  validates :last_name, presence: true

  # Add associations to make it easier to access data
  belongs_to :user_type
  has_many :organisation_users, dependent: :destroy
  has_many :organisations, through: :organisation_users
  has_many :course_users, dependent: :destroy
  has_many :courses, through: :course_users

  # Public Methods

  # This method should return a string can joins the first and last name
  def full_name
    [first_name, last_name].join(" ")
  end

  # This method will allow us to check if the user can edit an organisation
  def can_edit_organisation?(organisation)
    # Return the value of this if statement. The user will need to be able to
    # either be the owner or a user who can edit the organisation
    organisation.owner_id == id || organisation_users.where(can_edit: true).pluck(:organisation_id).include?(organisation.id)
  end

  # This method will get the users most recent courses
  # The limit is defaulted to four
  def recent_courses(limit=4)
    # Get all the courses using the association of course users
    course_users.order(updated_at: :desc).limit(limit).map{ |course| course.course.for_json }
  end

  # This method will get the user ready as JSON
  def for_json
    {
      first_name: first_name,
      last_name: last_name,
      full_name: full_name,
      email: email,
      user_type: user_type.name
    }
  end
end
