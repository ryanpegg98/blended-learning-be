class Block < ApplicationRecord
  # Validations
  validates :section, presence: true
  validates :block_type, presence: true
  validates :show_in_slides, presence: true, allow_blank: true
  validates :rank, presence: true

  # Associations
  belongs_to :section
  belongs_to :block_type
  has_many :block_configs, dependent: :destroy

  # Scopes
  scope :in_order, -> { order(rank: :asc) }
  scope :in_presentation, -> { where(show_in_slides: true) }

  # Method to return a hash ready for JSON
  def for_json
    {
      id: id,
      show_in_slides: true,
      rank: rank,
      block_type: block_type,
      configs: block_configs.map{ |config| config.for_json }
    }
  end

  # This will be the method that is slightly different to the typical for json as it
  # alters the way the config values are returned
  def for_show_json
    {
      id: id,
      show_in_slides: true,
      rank: rank,
      block_type: block_type,
      configs: configs_for_json
    }
  end

  # This method will create a hash of the configs
  def configs_for_json
    configs = {}

    block_configs.each do |config|
      configs[config.config_name.to_sym] = config.value
    end

    configs
  end

  # This method will be used to move the block up the list by changing the rank
  def move_up!
    # Create the new rank
    new_rank = rank - 1

    # Find the blocks with the new rank and make them all the current rank
    section.blocks.where(rank: new_rank).update_all(rank: rank)
    # Now change the block to be the new rank
    update(rank: new_rank)
    # Reassign the ranks
    reassign_ranks!
  end

  # This method will be used to move the block down the list by changing the rank
  def move_down!
    # Create the new rank
    new_rank = rank + 1

    # Find the blocks with the new rank and make them all the current rank
    section.blocks.where(rank: new_rank).update_all(rank: rank)
    # Now change the block to be the new rank
    update(rank: new_rank)
    # Reassign the ranks
    reassign_ranks!
  end

  # This method will be called to reassign the ranks to the blocks to make sure they
  # start from 0
  def reassign_ranks!
    section.blocks.in_order.each_with_index do |block, index|
      block.update(rank: index)
    end
  end

  # This method will be used to get the the next block in the presentation
  def next_in_presentation
    blocks = section.lesson.blocks.in_presentation.joins(:section).where("`sections`.`lesson_id` = ? AND ( `sections`.`rank` = #{section.rank} AND `blocks`.`rank` > #{rank} OR `sections`.`rank` > #{section.rank} )", section.lesson_id).order("`sections`.`rank` ASC, `blocks`.`rank` ASC")
    if blocks.any?
      blocks.first
    else
      nil
    end
  end

  def previous_in_presentation
    blocks = section.lesson.blocks.in_presentation.joins(:section).where("`sections`.`lesson_id` = ? AND ( `sections`.`rank` = #{section.rank} AND `blocks`.`rank` < #{rank} OR `sections`.`rank` < #{section.rank} )", section.lesson_id).order("`sections`.`rank` DESC, `blocks`.`rank` DESC")
    if blocks.any?
      blocks.first
    else
      nil
    end
  end
end
