class UserType < ApplicationRecord
  # Validations to check the record is in a usable format
  validates :name, presence: true, uniqueness: true

  # Add associations to make it easier to access data
  has_many :users

  # Create methods that can be used on a model

  # This method will be used to get the super admin user type
  def self.super_admin
    find_or_create_by(name: 'Super Admin')
  end

  # This method will be used to get the admin user type
   def self.admin
    find_or_create_by(name: 'Admin')
  end

  # This method will be used to get the teacher user type
  def self.teacher
    find_or_create_by(name: 'Teacher')
  end

  # This method will be used to get the learner user type
  def self.learner
    find_or_create_by(name: 'Learner')
  end
end
