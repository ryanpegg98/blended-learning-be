class BlockType < ApplicationRecord
  # Include concerns
  include OptionsForConfig

  # Validations
  validates :name, presence: true
  validates :system_name, presence: true

  # Constants for the blocks that are created
  BANNER_BLOCK = {
    name: 'banner',
    configs: [
      { name: 'text', type: 'text', default: "Header" },
      { name: 'background_colour', type: 'colour_select', default: 'grey' },
      { name: 'text_colour', type: 'colour_select', default: 'black' },
      { name: 'heading_type', type: 'heading_select', default: 'h2' }
    ]
  }

  IMAGE_BLOCK = {
    name: 'image_block',
    configs: [
      { name: 'image_link', type: 'text', default: "" },
      { name: 'image_description', type: 'text', default: "" },
      { name: 'caption', type: 'text', default: "" }
    ]
  }

  VIDEO_BLOCK = {
    name: 'video_block',
    configs: [
      { name: 'video_link', type: 'text', default: "" },
      { name: 'caption', type: 'text', default: "" }
    ]
  }

  IMAGE_TEXT_BLOCK = {
    name: 'image_text_block',
    configs: [
      { name: 'image_link', type: 'text', default: "" },
      { name: 'image_description', type: 'text', default: "" },
      { name: 'caption', type: 'text', default: "" },
      { name: 'text', type: 'markdown', default: "" },
      { name: 'background_colour', type: 'colour_select', default: 'grey' },
      { name: 'text_colour', type: 'colour_select', default: 'black' },
      { name: 'image_on_left', type: 'boolean', default: 'false' }
    ]
  }

  LINK_BLOCK = {
    name: 'link_block',
    configs: [
      { name: 'link_path', type: 'text', default: "" },
      { name: 'link_text', type: 'text', default: "" },
      { name: 'background_colour', type: 'colour_select', default: 'grey' },
      { name: 'text_colour', type: 'colour_select', default: 'black' }
    ]
  }

  TEXT_BLOCK = {
    name: 'text_block',
    configs: [
      { name: 'text', type: 'markdown', default: "" },
      { name: 'background_colour', type: 'colour_select', default: 'grey' },
      { name: 'text_colour', type: 'colour_select', default: 'black' }
    ]
  }

  QUOTE_BLOCK = {
    name: 'quote_block',
    configs: [
      { name: 'quote', type: 'text', default: "Quote Text" },
      { name: 'author', type: 'text', default: 'Author' },
      { name: 'background_colour', type: 'colour_select', default: 'grey' },
      { name: 'text_colour', type: 'colour_select', default: 'black' }
    ]
  }

  # This method will return the settings that will be applied to the block
  def configs
    case system_name
      when BANNER_BLOCK[:name]
        BANNER_BLOCK[:configs]
      when IMAGE_BLOCK[:name]
        IMAGE_BLOCK[:configs]
      when VIDEO_BLOCK[:name]
        VIDEO_BLOCK[:configs]
      when IMAGE_TEXT_BLOCK[:name]
        IMAGE_TEXT_BLOCK[:configs]
      when LINK_BLOCK[:name]
        LINK_BLOCK[:configs]
      when TEXT_BLOCK[:name]
        TEXT_BLOCK[:configs]
      when QUOTE_BLOCK[:name]
        QUOTE_BLOCK[:configs]
    end
  end

  # This method will return the block ready for JSON
  def for_json
    {
      id: id,
      name: name,
      system_name: system_name,
      configs: configs.map{ |config| config_for_json(config) }
    }
  end

  # This method will get the config ready for JSON, with the settings and the options
  def config_for_json(config)
    {
      friendly_name: config[:name].humanize,
      name: config[:name],
      type: config[:type],
      default: config[:default],
      options: options_for_config_type(config[:type])
    }
  end

  def for_show_json
    {
      id: id,
      name: name,
      system_name: system_name,
      configs: configs_for_show_json
    }
  end

  def configs_for_show_json
    config_types = {}

    configs.each do |config|
      config_types[config[:name].to_sym] = {
        name: config[:name].humanize,
        type: config_type(config[:type]),
        default: config[:default],
        options: options_for_config_type(config[:type])
      }
    end

    config_types
  end

  # Get the type but make all selects act the same
  def config_type(type)
    # if it includes select the type should be select
    if type.include?('select')
      'select'
    else
      # Otherwise the show the normal type
      type
    end
  end
end
