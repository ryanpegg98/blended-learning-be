class OrganisationUser < ApplicationRecord
  # Validations to ensure that all the data that is required is present
  validates :organisation_id, presence: true
  validates :user_id, presence: true, uniqueness: { scope: :organisation_id }
  validates :can_edit, inclusion: [true, false]

  # Assocications that will be used
  belongs_to :organisation
  belongs_to :user
end
