class Lesson < ApplicationRecord
  include Snippets

  # Validations
  validates :title, presence: true
  validates :course, presence: true
  validates :rank, presence: true

  # Associations
  belongs_to :course
  has_many :sections, dependent: :destroy
  has_many :blocks, through: :sections

  # Scopes
  scope :in_order, -> { order(rank: :asc) }

  # Method to move the lesson up the list of lessons
  def move_up!
    # Set the new rank
    new_rank = rank - 1
    # Get the course that is currently the new rank and change it to this lessons rank
    course.lessons.find_by(rank: new_rank).update(rank: rank)
    # Now change the rank to be the new rank
    update(rank: new_rank)
    # Reassign the ranks for the tasks
    reassign_ranks
  end

  def move_down!
    # Create the new rank by increasing the rank
    new_rank = rank + 1

    # swap the lesson with the new rank to have th eold rank
    course.lessons.find_by(rank: new_rank).update(rank: rank)
    # Change the rank to the new rank
    update(rank: new_rank)

    # Reassign the ranks to ensure they are in the correct order
    reassign_ranks
  end

  def reassign_ranks
    # Loop over each of the lessosn in the order they are correct
    course.lessons.in_order.each_with_index do |lesson, index|
      # Change the rank to be that same as the index of the array position
      lesson.update(rank: index)
    end
  end

  # Method to return the object as a hash so that all the information is available
  def for_json
    {
      id: id,
      title: title,
      description: description,
      snippet: snippet,
      rank: rank,
      sections: sections.in_order
    }
  end

  # This method will get the data that will be used by the presentation method to get all the
  # data for the presentation
  def for_present_json
    {
      id: id,
      title: title,
      description: description,
      rank: 0,
      sections: sections.in_presentation
    }
  end

  # Method to show give the first block for the lesson
  def first_block
    ordered_blocks = blocks.in_presentation.joins(:section).order("`sections`.`rank` ASC, `blocks`.`rank` ASC")

    if ordered_blocks.any?
      ordered_blocks.first
    else
      ''
    end
  end

  # Method to show give the last block for the lesson
  def last_block
    reverse_ordered_blocks = blocks.in_presentation.joins(:section).order("`sections`.`rank` DESC, `blocks`.`rank` DESC")

    if reverse_ordered_blocks.any?
      reverse_ordered_blocks.first
    else
      ''
    end
  end
end
