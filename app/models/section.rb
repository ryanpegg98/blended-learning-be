class Section < ApplicationRecord
  # Validations
  validates :lesson, presence: true
  validates :title, presence: true
  validates :rank, presence: true

  # Associations
  belongs_to :lesson, class_name: "Lesson"
  has_many :blocks, dependent: :destroy

  # Scopes
  scope :in_order, -> { order(rank: :asc) }
  scope :last_section, -> { order(rank: :desc) }
  scope :in_presentation, -> { joins(:blocks).where(blocks: { show_in_slides: true }).distinct }

  # This method will move the section up the ranks
  def move_up!
    # Set the new rank as a variable
    new_rank = rank - 1
    # Set the section with that rank to be the old rank
    old_section = lesson.sections.where(rank: new_rank)
    # If any have been found they will need to be changed
    old_section.update_all(rank: rank) if old_section.any?

    # Check that the new_rank is no lower than 0
    if new_rank >= 0
      # Update the rank to be the new rank
      update(rank: new_rank)
    end
  end

  # This method will move the section down
  def move_down!
    # Set the new rank as a variable
    new_rank = rank + 1
    # Set the section with that rank to be the old rank
    old_section = lesson.sections.where(rank: new_rank)
    # If any have been found they will need to be changed
    old_section.update_all(rank: rank) if old_section.any?

    # Check that the new_rank is no lower than 0
    if new_rank >= 0
      # Update the rank to be the new rank
      update(rank: new_rank)
    end
  end

  # This method will ensure that all of the ranks have the appropriate rank
  def reassign_ranks
    # Loop over all of the sections for the lesson in order and assign them the index
    # as a rank
    lesson.sections.in_order.each_width_index do |section, index|
      section.update(rank: index)
    end
  end

  # This method will show a section with all the values needed for JSON
  def for_json
    {
      id: id,
      title: title,
      rank: rank,
      blocks: blocks.in_order.map{ |block| block.for_show_json }
    }
  end

  # This method will get the next section for the lesson
  def previous_section
    # Get the sections where the rank is less than the sections rank
    # Put them in reverse order to get the actual previous section
    previous_sections = lesson.sections.where("`sections`.`rank` < ?", rank).order(rank: :desc)
    # If there are any sections return the first one
    if previous_sections.any?
      # Return the previous section by getting the first result
      previous_sections.first
    else
      # If none are found return nil
      nil
    end
  end

  # This method will get the next section for the lesson
  def next_section
    # Get the sections where the rank is more than the sections rank
    next_sections = lesson.sections.where("`sections`.`rank` > ?", rank).order(rank: :asc)
    # If there are any sections return the first one
    if next_sections.any?
      # Return the next section by getting the first result
      next_sections.first
    else
      # If none are found return nil
      nil
    end
  end
end
