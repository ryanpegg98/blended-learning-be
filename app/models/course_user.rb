class CourseUser < ApplicationRecord
  # Validations
  validates :course, presence: true
  validates :user, presence: true
  validates :can_edit, inclusion: [true, false]

  # Associations
  belongs_to :course
  belongs_to :user
end
