class BlockConfig < ApplicationRecord
  # Include concerns
  include OptionsForConfig

  # Validations
  validates :block, presence: true
  validates :config_name, presence: true

  # Associations
  belongs_to :block
  has_one :block_type, through: :block

  # This method will return the value of a config ready for use in JSON
  def for_json
    # Get the config hash from the value
    config_hash = get_config_hash
    # Return the hash with the content from the default hash and the current result
    {
      config_name: config_name,
      value: value,
      type: config_hash[:type],
      default: config_hash[:default],
      options: options_for_config_type(config_hash[:type])
    }
  end

  def get_config_hash
    # Get the block type and go over each config
    block_type.configs.each do |config|
      # Return the config if the name in the hash matches the config_name
      return config if config[:name] == config_name
    end
    # Return a blank value if no config found
    { name: '', type: 'text', default: '' }
  end
end
