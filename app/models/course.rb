class Course < ApplicationRecord
  include Snippets

  # Validations
  validates :name, presence: true
  validates :owner, presence: true
  validates :organisation, presence: true
  validates :active, inclusion: [true, false]
  validates :sign_up_mode, presence: true
  validates :sign_up_code, allow_blank: true, uniqueness: true

  # Enums
  enum sign_up_mode: [:hidden, :password, :code_only, :open]

  # Associations
  belongs_to :organisation
  belongs_to :owner, class_name: 'User'
  has_many :course_users, dependent: :destroy
  has_many :users, through: :course_users, dependent: :destroy
  # Create an association to get all of the teachers
  has_many :teacher_users, -> { where(can_edit: true) }, class_name: 'CourseUser'
  has_many :teachers, through: :teacher_users, source: :user
  # Create an association to get all of the leaners
  has_many :learner_users, -> { where(can_edit: false) }, class_name: 'CourseUser'
  has_many :learners, through: :learner_users, source: :user
  # Add an association to get all of the lessons for the corse
  has_many :lessons, dependent: :destroy

  # Callbacks
  after_create :add_owner_to_users # Adds the owner to the link table

  # This method will add the owner to the link class
  def add_owner_to_users
    # Attempt to find the record and if not able to find it initialize a new instance
    new_user = course_users.find_or_initialize_by(user_id: owner_id)
    # Ensure the user can edit the course
    new_user.can_edit = true
    # Save the record so that the user can edit the course
    new_user.save

    # Get the owner of the organisation and add them automatically as a teacher
    organisation_owner_user = course_users.find_or_initialize_by(user_id: organisation.owner_id)
    # Make sure that the user can edit the course
    organisation_owner_user.can_edit = true
    # Save the user
    organisation_owner_user.save
  end

  # This method will create the hash for how a course will be renderd for JSON
  def for_json(user = nil)
    {
      id: id,
      name: name,
      description: description,
      snippet: snippet,
      active: active,
      sign_up_mode: sign_up_mode,
      sign_up_code: return_sign_up_code(user),
      can_edit: can_edit?(user),
      can_view: can_view?(user),
      owner: owner,
      organisation: organisation,
      teachers: teachers,
      learners: learners,
      lessons: lessons.in_order.map{ |lesson| lesson.for_json }
    }
  end

  def return_sign_up_code(user)
    return nil unless can_edit?(user)

    sign_up_code
  end

  def can_edit?(user)
    # If no user return false
    return false unless user.present?
    # Return the value of if they are in the teachers list
    teachers.pluck(:id).include?(user.id)
  end

  def can_view?(user)
    # If no user return false
    return false unless user.present?
    # Return the value of if they are in the users list
    users.pluck(:id).include?(user.id)
  end

  # This will get a list of users that can be added to the course
  def available_users
    # Get all the users in the organisation that are not already added to the course
    organisation.organisation_users.joins(:user).where.not(organisation_users: { user_id: course_users.pluck(:user_id) }).order(first_name: :asc, last_name: :asc).map{ |user|
      {
        can_edit: user.can_edit,
        id: user.user_id,
        first_name: user.user.first_name,
        last_name: user.user.last_name,
        email: user.user.email
      }
    }
  end
end
