module Snippets
  extend ActiveSupport::Concern

  included do
    # This method will create a snippet of the description
    def snippet(length=100)
      return nil unless description.present?
      Redcarpet::Markdown.new(Redcarpet::Render::StripDown).render(description).truncate(length)
    end
  end
end
