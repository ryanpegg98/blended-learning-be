module OptionsForConfig
  extend ActiveSupport::Concern

  included do
    def options_for_config_type(type)
      case type
        when 'heading_select'
          heading_options
        when 'colour_select'
          colour_options
        when 'boolean'
          boolean_options
        else
          []
      end
    end

    def colour_options
      [
        { value: 'white', label: 'White' },
        { value: 'black', label: 'Black' },
        { value: 'navy', label: 'Navy' },
        { value: 'grey', label: 'Grey' },
        { value: 'pink', label: 'Pink' },
        { value: 'purple', label: 'Purple' },
        { value: 'blue', label: 'Blue' },
        { value: 'green', label: 'Green' },
        { value: 'yellow', label: 'Yellow' },
        { value: 'orange', label: 'Orange' },
        { value: 'red', label: 'red' }
      ]
    end

    def heading_options
      [
        { value: 'h2', label: 'Heading 1' },
        { value: 'h3', label: 'Heading 2' },
        { value: 'h4', label: 'Heading 3' },
        { value: 'h5', label: 'Heading 4' },
        { value: 'h6', label: 'Heading 5' },
      ]
    end

    def boolean_options
      [
        { value: 'true', label: "Yes" },
        { value: 'false', label: "No" }
      ]
    end
  end
end
