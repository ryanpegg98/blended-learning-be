require 'redcarpet/render_strip'

class Organisation < ApplicationRecord
  include Snippets

  # Validations
  validates :name, presence: true, uniqueness: true
  validates :owner_id, presence: true
  validates :sign_up_mode, presence: true, allow_blank: true

  # Associations
  belongs_to :owner, class_name: "User"
  has_many :organisation_users, dependent: :destroy
  has_many :users, through: :organisation_users
  has_many :courses, dependent: :destroy

  # Callbacks
  after_create :add_owner_to_users

  # Enums
  enum sign_up_mode: [:no_sign_up, :code] # This will help to define how a user will be able to sign up to the organisation

  # Scopes

  # Scope to get an organisation with the correct sign up code
  scope :via_sign_up_code, ->(code) {
    where.not(sign_up_mode: :no_sign_up, sign_up_code: nil)# Check that the organisation has enabled the signup
      .where(sign_up_code: code) # Try and find the organisation using the code supplied
  }

  # This method will return a hash that will be used in for json
  def for_json
    {
      id: id,
      name: name,
      description: description,
      snippet: snippet(100),
      sign_up_mode: sign_up_mode,
      sign_up_code: sign_up_code,
      users: users,
      owner: owner
    }
  end

  private

  # This method will be used to create the record for the owner as a callback
  def add_owner_to_users
    # Try to find the join record or initialize one to be added
    join_record = organisation_users.find_or_initialize_by(user_id: owner_id)
    # Ensure that they can edit the application
    join_record.can_edit = true
    # Save the record so that the user can access the organisation
    join_record.save
  end
end
