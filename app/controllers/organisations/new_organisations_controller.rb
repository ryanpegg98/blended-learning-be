class Organisations::NewOrganisationsController < ApplicationController
  def create
    # Check that the organisation name is valid
    organisation_name = params.dig(:organisation, :name)
    # Store all the errors for the organisation
    organisation_errors = []

    # Check the name is present
    unless organisation_name.present?
      # Add the error message to the array
      organisation_errors << "can't be blank"
    end

    # Check that the name is unique
    if organisation_errors.empty? && Organisation.where(name: organisation_name).any?
      # Add the error message to the array
      organisation_errors << 'has already been taken'
    end

    # Check that the user is valid
    @user = User.new(user_params)
    @user.user_type = UserType.admin
    # Ensure the password confirmation is set
    @user.password_confirmation = ' ' if @user.password_confirmation.blank?

    # Check the user is valid before creating the user or organisation
    if @user.valid? && organisation_errors.empty?
      # Create the instance for the organisation
      @organisation = Organisation.new(name: organisation_name)
      # Attempt to save the user and the organisation
      if @user.save && save_organistion
        # Render the JSON with the user and the organisation
        render json: { user: @user, organisation: @organisation }
      else
        # Combine the errors from the user and any from the organisation
        errors = @user.errors
        errors[:organisation_name] = @organisation.errors[:name] if @organisation.errors.messages.any?
        # Return the errors with the status
        render json: { errors: errors }, status: 422
      end
    else
      # Combine the errors from the user and the organisations errors variable
      errors = @user.errors.messages
      errors[:organisation_name] = organisation_errors if organisation_errors.any?
      # Return the errors with the correct status
      render json: { errors: errors }, status: 422
    end
  end

  private

  # This method will only get the data that can be changed
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  def save_organistion
    # Assign the new user as the owner
    @organisation.owner_id = @user.id
    # Save the organisation
    @organisation.save
  end
end
