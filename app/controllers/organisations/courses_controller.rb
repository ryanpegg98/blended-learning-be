class Organisations::CoursesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation
  before_action :set_course, only: [:show, :update, :destroy]

  def index
    # fetch all the courses and
    @courses = @organisation.courses.order(name: :asc)
    # Return the oganisation and the courses
    render json: { organisation: format_organisation, courses: @courses }
  end

  def create
    # Create an instance of a new course for the orgaisation
    @course = @organisation.courses.new(course_params)
    @course.owner_id = current_user.id # Assign the current user as the owner

    # Check that the value was saved
    if @course.save
      # Return the orgaisation and the course that was created
      render json: { organisation: format_organisation, course: @course }
    else
      # If it fails to save the errors should be returned with the correct status
      render json: { errors: @course.errors }, status: 422
    end
  end

  def show
    # Render the course using the method on the model
    render json: { course: @course.for_json(current_user)  }
  end

  def update
    # Attempt to update the course
    if @course.update(course_params)
      # Show the course using the method on the model
      render json: { course: @course.for_json(current_user) }
    else
      # If it fails sent the errors back to the user
      render json: { errors: @course.errors }, status: 422
    end
  end

  def destroy
    # Attempt to destroy the record that has been requested
    if @course.destroy
      # Return he message that the record has been deleted
      render json: { message: "Course deleted", course: @course }
    else
      # Return a message saying the organisation could not be deleted
      render json: { errors: [ 'Unable to delete course' ], course: @course }, status: 422
    end
  end

  # This method will allow a user to join the course
  def join_course
    # Get the requested course
    courses = @organisation.courses.where(id: params[:course_id])
    # Check that a course has been passed back
    if courses.any?
      # Get the first course
      @course = courses.first
    else
      # If any no courses have been passed back return 404
      return render json: { error: 'Could not find requested course' }, status: 404
    end


    # Check to see if the course is an open one
    if @course.sign_up_mode == 'open'
      # Run the method to add the current user to the course
      add_user_to_course
    elsif @course.sign_up_mode == 'password'
      # Check that the code matches the course code
      password = params.dig(:course, :code)
      if @course.sign_up_code == password
        add_user_to_course
      else
        # If it doesn't match then send the error back
        return render json: { errors: { code: [ 'does not match the course' ] } }, status: 422
      end
    end
  end

  # This method will get the users that can be added to a course
  def available_users
    # Get the requested course
    @course = @organisation.courses.find(params[:course_id])

    # Check that the course has been found
    if @course.present?
      render json: {
        course: @course.for_json(current_user),
        available_users: @course.available_users
      }
    else
      # Return the 404 message
      render json: { error: 'Failed to find the course' }, status: 404
    end
  end

  # This method will add a user as a teacher
  def add_teacher
    # Get the requested course
    @course = @organisation.courses.find(params[:course_id])
    # Check that the course has been found
    if @course.present?
      user_id = params.dig(:user_id)
      # Only add the user if the id has been supplied
      if user_id.present?
        # Try to find the course user or initialize a new one
        course_user = @course.course_users.find_or_initialize_by(user_id: user_id)
        # Ensure that the can edit is changed to true
        course_user.can_edit = true
        # Save the course user
        course_user.save
      end

      # Render the JSON again
      render json: {
        course: @course.for_json(current_user),
        available_users: @course.available_users
      }
    else
      # Return the 404 message
      render json: { error: 'Failed to find the course' }, status: 404
    end
  end

  # This method will add a user as a learner
  def add_learner
    # Get the requested course
    @course = @organisation.courses.find(params[:course_id])
    # Check that the course has been found
    if @course.present?
      user_id = params.dig(:user_id)
      # Only add the user if the id has been supplied
      if user_id.present?
        # Try to find the course user or initialize a new one
        course_user = @course.course_users.find_or_initialize_by(user_id: user_id)
        # Ensure that the can edit is changed to false
        course_user.can_edit = false
        # Save the course user
        course_user.save
      end

      # Render the JSON again
      render json: {
        course: @course.for_json(current_user),
        available_users: @course.available_users
      }
    else
      # Return the 404 message
      render json: { error: 'Failed to find the course' }, status: 404
    end
  end

  # This method will allow the users to edit the course as a teacher
  def promote
    # Get the orgaisation that has been requested
    @course = @organisation.courses.find(params[:course_id])

    # Get the user
    user_id = params.dig(:user_id)
    if user_id.present?
      # Get the user
      @user = @course.course_users.find_by(user_id: user_id)

      # Check that the user has been found
      if @user.present?
        # Turn the edit on the association
        @user.update(can_edit: true)
        # Return the json for the organisation
        render json: { course: @course.for_json(current_user) }
      else
        # Send the 404 message
        render json: { error: 'Failed to find the user' }, status: 404
      end
    else
      # Return the error message
      render json: { errors: { user_id: [ 'cannot be blank' ] } }, status: 422
    end
  end

  # This method will remove the permissions to edit the course
  def demote
    # Get the orgaisation that has been requested
    @course = @organisation.courses.find(params[:course_id])

    # Get the user
    user_id = params.dig(:user_id)
    if user_id.present?
      # Get the user
      @user = @course.course_users.find_by(user_id: user_id)

      # Check that the user has been found
      if @user.present?
        # Turn the edit off on the association
        @user.update(can_edit: false)
        # Return the json for the organisation
        render json: { course: @course.for_json(current_user) }
      else
        # Send the 404 message
        render json: { error: 'Failed to find the user' }, status: 404
      end
    else
      # Return the error message
      render json: { errors: { user_id: [ 'cannot be blank' ] } }, status: 422
    end
  end

  private

  # This method will join the user to the course
  def add_user_to_course
    # Add the learner to the course
    @course.learner_users.find_or_create_by(user_id: current_user.id)

    # Return the course ready for the typical JSON response
    return render json: { course: @course.for_json(current_user) }
  end

  # This method will set the organisation on every request
  def set_organisation
    @organisation = current_user.organisations.find(params[:organisation_id])

    return render json: { error: "Could not find organisation" }, status: 404 unless @organisation.present?
  end

  # This method will be used to get the course the user is requesting
  def set_course
    # Get the course from the list of the organisations courses
    @course = @organisation.courses.find(params[:id])

    return render json: { error: "Could not find course", organisation: @organisation }, status: 404 unless @course.present?
  end

  # This method will get the data from the params for the course
  def course_params
    params.require(:course).permit(:name, :description, :active, :sign_up_mode, :sign_up_code)
  end

  # This method will be used to structure the orgaisation value and how it is returned
  def format_organisation
    {
      id: @organisation.id,
      name: @organisation.name,
      description: @organisation.description,
      sign_up_mode: @organisation.sign_up_mode,
      sign_up_code: @organisation.sign_up_code,
      user_can_edit: current_user.can_edit_organisation?(@organisation),
      users: @organisation.users,
      owner: @organisation.owner
    }
  end
end
