class Organisations::LessonsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation
  before_action :set_course
  before_action :set_lesson, only: [:show, :update, :destroy]

  def create
    @lesson = @course.lessons.new(lesson_params)
    # Makesure the lesson's rank is the last one
    lessons = @course.lessons.order(rank: :desc)
    if lessons.any?
      # Get the last lesson and increment the rank to make the new lesson rank
      @lesson.rank = lessons.first.rank + 1
    else
      # If this is the first lesson then it should just be 0
      @lesson.rank = 0
    end
    # Attempt to save the lesson
    if @lesson.save
      # Return the lesson with the
      render json: { lesson: @lesson, course: @course.for_json }
    else
      # Show errors for the object
      render json: { errors: @lesson.errors }, status: 422
    end
  end

  def show
    render json: { lesson: @lesson.for_json, course: @course.for_json(current_user) }
  end

  def update
    if @lesson.update(lesson_params)
      # Return the lesson with the
      render json: { lesson: @lesson, course: @course.for_json(current_user) }
    else
      # Show errors for the object
      render json: { errors: @lesson.errors }, status: 422
    end
  end

  def destroy
    if @lesson.destroy
      render json: { lessons: @course.lessons }
    else
      render json: { message: "Unable to delete lesosn" }, status: 422
    end
  end

  def move_up
    @lesson = @course.lessons.find(params[:lesson_id])

    if @lesson.present?
      @lesson.move_up!

      render json: { course: @course.for_json(current_user) }
    else
      render json: { error: "Unable to find the lesson" }, status: 404
    end
  end

  def move_down
    @lesson = @course.lessons.find(params[:lesson_id])

    if @lesson.present?
      @lesson.move_down!

      render json: { course: @course.for_json(current_user)  }
    else
      render json: { error: "Unable to find the lesson" }, status: 404
    end
  end

  private

  # This methos will pass back all the data a user can change on a lesson
  def lesson_params
    params.require(:lesson).permit(:title, :description, :rank)
  end

  # This method will be used to get the organisation using the params
  def set_organisation
    # Attempt to find the course
    @organisation = current_user.organisations.find(params[:organisation_id])
    # if the organisation could not be found return the error message
    return render json: { error: 'Unable to find organisation' }, status: 404 unless @organisation.present?
  end

  # This method will be used to get the course using the params
  def set_course
    # Attempt to find the course from the organisation
    @course = @organisation.courses.find(params[:course_id])
    # if the course could not be found return the error message
    return render json: { error: 'Unable to find course' }, status: 404 unless @organisation.present?
  end

  # This method will be used to get the requested lesson
  def set_lesson
    # Attempt to get the lesson from the course
    @lesson = @course.lessons.find(params[:id])
    # Return error if the course could not be found
    return render json: { error: 'Unable to find lesson' }, status: 404 unless @lesson.present?
  end
end
