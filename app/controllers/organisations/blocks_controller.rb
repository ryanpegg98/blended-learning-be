class Organisations::BlocksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation
  before_action :set_course
  before_action :set_lesson
  before_action :set_section
  before_action :set_block, only: [:show, :update, :destroy]

  def create
    # last rank either the last rank value plus one or zero
    last_rank = @section.blocks.any? ? @section.blocks.order(rank: :desc).first.rank + 1 : 0
    # Create an instance of the block
    @block = @section.blocks.new(block_params)
    # Assign the block the rank
    @block.rank = last_rank
    # Check that the block can be saved before adding the configs
    if @block.save
      @block.block_type.configs.each do |config|
        # Attempt to find the config name value in the
        config_value = params.dig(:configs, config[:name].to_sym)
        # Attempt to find or initialize the config item
        block_config = @block.block_configs.find_or_initialize_by(config_name: config[:name])

        if config_value.present?
          block_config.value = config_value
        else
          block_config.value = config[:default]
        end

        block_config.save
      end

      render json: { block: @block.for_json, section: @section, lesson: @lesson, course: @course.for_json(current_user)  }
    else
      render json: { errors: @block.errors }, status: 422
    end
  end

  def show
    render json: {
      block: @block.for_show_json,
      section: @section.for_json,
      lesson: @lesson,
      course: @course.for_json,
      block_type: @block.block_type.for_show_json
    }
  end

  def update
    # Attempt to update the blocks values
    if @block.update(block_params)
      # Loop over all the configs for the block type
      @block.block_type.configs.each do |config|
        config_value = params.dig(:configs, config[:name].to_sym)
        block_config = @block.block_configs.find_or_initialize_by(config_name: config[:name])
        block_config.value = config_value

        block_config.save
      end
      # Render the block for json
      render json: { block: @block.for_json }
    else
      # Show the errors and return the error code
      render json: { errors: @block.errors }, status: 422
    end
  end

  def destroy
    # Attempt to delete the block
    if @block.destroy
      # Send the section and all the block back
      render json: { section: @section.for_json }
    else
      # If it fails send the error and send the correct status back
      render json: { error: "Unable to delete block" }, status: 422
    end
  end

  # This method will move a block up the list of blocks
  def move_up
    # Find the block that will be moved
    @block = @section.blocks.find(params[:block_id])
    # Check that the block could be found
    if @block.present?
      # Call the method to move the block up
      @block.move_up!

      # render the JSON for the section
      render json: { section: @section.for_json }
    else
      # Render the 404 message
      render json: { error: "Could not find the requested block" }, status: 404
    end
  end

  # This method will move a block down the list of blocks
  def move_down
    # Find the block that will be moved
    @block = @section.blocks.find(params[:block_id])
    # Check that the block could be found
    if @block.present?
      # Call the method to move the block down
      @block.move_down!

      # render the JSON for the section
      render json: { section: @section.for_json }
    else
      # Render the 404 message
      render json: { error: "Could not find the requested block" }, status: 404
    end
  end

  private

  # This method will control the values users can change on the block item
  def block_params
    params.require(:block).permit(:block_type_id, :show_in_slides, :rank)
  end

  # This will get the block based on the section and the params
  def set_block
    # Set the block based on the already found section
    @block = @section.blocks.find(params[:id])
    # Return a 404 if the block could not be found
    return render json: { error: "Could not find the block" }, status: 404 unless @block.present?
  end

  # This method wil get the organisation so long as the user can access the organisation
  def set_organisation
    # Attempt to get the organisation
    @organisation = current_user.organisations.find(params[:organisation_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the organisation" }, status: 404 unless @organisation.present?
  end

  # This method will get the course using the params and the organisation model
  def set_course
    # Attempt to get the organisation
    @course = @organisation.courses.find(params[:course_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the course" }, status: 404 unless @course.present?
  end

  # This method will get the lesson using the params and the course model
  def set_lesson
    # Attempt to get the organisation
    @lesson = @course.lessons.find(params[:lesson_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the lesson" }, status: 404 unless @lesson.present?
  end

  # This will get the section that has been requested
  def set_section
    # This will get the section that has been requested based upon the lesson
    @section = @lesson.sections.find(params[:section_id])
    # If the section could not be found then return the 404 error
    return render json: { error: "Could not find the section" }, status: 404 unless @section.present?
  end
end
