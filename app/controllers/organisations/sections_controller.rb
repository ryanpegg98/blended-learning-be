class Organisations::SectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_organisation
  before_action :set_course
  before_action :set_lesson
  before_action :set_section, only: [:show, :update, :destroy]

  def create
    # Get all of the current sections before a new one is added
    sections = @lesson.sections.in_order
    # Create an instance of a new section
    @section = @lesson.sections.new(section_params)
    # Assign the rank that is after the last section if any exist
    if sections.any?
      @section.rank = sections.last.rank + 1
    else
      @section.rank = 0
    end

    # Attempt to save the section
    if @section.save
      # Return the section that has been created
      render json: { section: @section, lesson: @lesson.for_json }
    else
      # return the errors and the status
      render json: { errors: @section.errors }, status: 422
    end
  end

  def show
    render json: { section: @section.for_json, lesson: @lesson.for_json, course: @course.for_json(current_user)  }
  end

  def update
    # Attempt to update the the section
    if @section.update(section_params)
      # Show the section that has been updated
      render json: { section: @section }
    else
      # Send the errors if the section could not be updated
      render json: { errors: @section.errors }, status: 422
    end
  end

  def destroy
    # Attempt to destroy the section
    if @section.destroy
      # Return the section
      render json: { sections: @lesson.sections.in_order }
    else
      # Render the error message with the 422 status code
      render json: { error: 'Failed to delete the section' }, status: 422
    end
  end

  # This method will move a section up the ranks
  def move_up
    # Find the section
    @section = @lesson.sections.find(params[:section_id])
    # Check tht the section could be found
    if @section.present?
      @section.move_up!
      # Return all the lessons in the correct order
      render json: { sections: @lesson.sections.in_order }
    else
      # Return the 404 message
      render json: { error: "Could not find the section" }, status: 404
    end
  end

  # This method will move a section down the ranks
  def move_down
    # Find the section
    @section = @lesson.sections.find(params[:section_id])
    # Check tht the section could be found
    if @section.present?
      @section.move_down!
      # Return all the lessons in the correct order
      render json: { sections: @lesson.sections.in_order }
    else
      # Return the 404 message
      render json: { error: "Could not find the section" }, status: 404
    end
  end

  private

  # This method will be get the values for the section that the user can update
  def section_params
    params.require(:section).permit(:title, :rank)
  end

  # This method will get the organisation using the params
  def set_organisation
    # Attempt to get the organisation
    @organisation = current_user.organisations.find(params[:organisation_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the organisation" }, status: 404 unless @organisation.present?
  end

  # This method will get the course using the params and the organisation model
  def set_course
    # Attempt to get the organisation
    @course = @organisation.courses.find(params[:course_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the course" }, status: 404 unless @course.present?
  end

  # This method will get the lesson using the params and the course model
  def set_lesson
    # Attempt to get the organisation
    @lesson = @course.lessons.find(params[:lesson_id])
    # Return the 404 status with an error message
    return render json: { error: "Could not find the lesson" }, status: 404 unless @lesson.present?
  end

  # This will get the section that has been requested
  def set_section
    # This will get the section that has been requested based upon the lesson
    @section = @lesson.sections.find(params[:id])
    # If the section could not be found then return the 404 error
    return render json: { error: "Could not find the section" }, status: 404 unless @section.present?
  end
end
