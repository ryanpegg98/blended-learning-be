class Organisations::OrganisationsController < ApplicationController
  # Validate that the user is logged in before they are allowed to see the data
  before_action :authenticate_user!
  # Before the show, update and destory we will need to get the record for the organisation
  before_action :set_organisation, only: [:show, :update, :destroy]

  def index
    # Fetch all of the organisations that the user is asscoiated with
    @organisations = current_user.organisations
    # Show the organisations as JSON
    render json: { organisations: @organisations.map{ |organisation| organisation.for_json } }
  end

  def create
    # Create a new organisation with the current user as the owner
    @organisation = Organisation.new(organisation_params)
    # Set the current user as the owner
    @organisation.owner = current_user

    # Check that the new organisation is valid
    if @organisation.save
      # render the organisation as JSON
      render json: { organisation: format_organisation }
    else
      # Return the status code of 422 and the json for all of the messages
      render json: { errors: @organisation.errors }, status: 422
    end
  end

  def show
    # Check that the organisation can be found
    if @organisation.present?
      # Show the organisation as JSON
      render json: { organisation: format_organisation }
    else
      # Set the status as 404 with the error of cannot find the organisation
      render json: { error: 'Cannot find the organisation' }, status: 404
    end
  end

  def update
    # Attempt to update the organisations using the params
    if @organisation.update(organisation_params)
      # Show the organisation as JSON
      render json: { organisation: format_organisation }
    else
      # Return the status code of 422 and the json for all of the messages
      render json: { errors: @organisation.errors }, status: 422
    end
  end

  def destroy
    # Attempt to destroy the record that has been requested
    if @organisation.destroy
      # Return he message that the record has been deleted
      render json: { message: "Organisation deleted", organisation: format_organisation }
    else
      # Return a message saying the organisation could not be deleted
      render json: { errors: [ 'Unable to delete organisation' ], organisation: format_organisation }, status: 422
    end
  end

  # This method will add a user to the organisation
  def link
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])
    # Check that the user has been added
    if @organisation.organisation_users.pluck(:user_id).include?(params[:user_id]) || @organisation.organisation_users.create(user_id: params[:user_id])
      # Return the users assciated with the organisation
      render json: @organisation.users
    else
      # Return the users assciated with the organisation and the error code
      render json: @organisation.users, status: 422
    end
  end

  # This method will be used to unlink a user with the organisation
  def unlink
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])
    # Get the association record
    record = @organisation.organisation_users.find_by(user_id: params[:user_id])
    # if the record has been deleted it should return a success
    if record.destroy
      # Return the users assciated with the organisation
      render json: @organisation.users
    else
      # Return the users assciated with the organisation and the error code
      render json: @organisation.users, status: 422
    end
  end

  # This method will allow a user to be create and linked to the the organisation as a teacher
  def create_teacher
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])

    # Create the teacher and assign them to the organisation
    @user = User.new(user_params)
    # Ensure the user is getting the teacher user type
    @user.user_type = UserType.teacher

    if @user.save
      # Add the user to the organisation
      @organisation.organisation_users.create(user_id: @user.id)
      render json: { organisation: format_organisation, user: @user }
    else
      render json: { errors: @user.errors.full_messages }, status: 422
    end
  end

  # This method will allow a learner user to be create and linked to the the organisation
  def create_learner
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])

    # Create the teacher and assign them to the organisation
    @user = User.new(user_params)
    # Ensure the user is getting the teacher user type
    @user.user_type = UserType.learner

    if @user.save
      # Add the user to the organisation
      @organisation.organisation_users.create(user_id: @user.id)
      render json: { organisation: format_organisation, user: @user }
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  # Use the get method to get the organisation the code belongs to
  def joining_code
    # Set the errors hash that will be used if there are any errors
    errors = { code: [] }
    # Find the code in the params but return nil if it is not found
    code = params.dig(:organisation, :code)
    # Check that the code has been suplied
    if code.present?
      # Run a query for the organisations looking for one that is available and has the correct code
      organisations = Organisation.via_sign_up_code(code)
    else
      errors[:code] << "is required"
    end

    # Check if the user is already a member of the organisation
    if organisations.any? && current_user.organisations.pluck(:id).include?(organisations.first.id)
      # Add an error message to the code
      errors[:code] << 'has already been used. You are currently a memeber of the organisation'
    elsif organisations.empty?
      errors[:code] << 'is not being used by any organisations'
    end

    # Check that the oranisation can be found and there are no errors
    if errors[:code].empty? && organisations.any?
      # Set th organisation
      @organisation = organisations.first
      # Returnt the organisation so they can be redirect to the correct page
      render json: { organisation: format_organisation }
    else
      # Return the error messages so the user know what went wrong
      # use the correct status code so the application shows the errors
      render json: { errors: errors }, status: 422
    end
  end

  # This method will get the organisation and add the user to the organisation
  def join_organisation
    errors = { code: [] }
    code = params.dig(:organisation, :code)
    if code.present?
      organisations = Organisation.via_sign_up_code(code)
    else
      errors[:code] << "is required"
    end

    # Check if the user is already a member of the organisation
    if organisations.any? && current_user.organisations.pluck(:id).include?(organisations.first.id)
      # Add an error message to the code
      errors[:code] << 'has already been used. You are currently a memeber of the organisation'
    elsif organisations.any?
      @organisation = organisations.first
    else
      errors[:code] << 'is not being used by any organisations'
    end

    # Check that the oranisation can be found and there are no errors
    if errors[:code].empty? && organisations.any?
      # Get the organisation
      @orgaisation = organisations.first
      # Add the user to the organisation
      current_user.organisation_users.find_or_create_by(organisation_id: @organisation.id)
      # Return the values of the organisation
      render json: { organisation: format_organisation }
    else
      # Return the errors with the status code of 422
      render json: { errors: errors }, status: 422
    end
  end

  # This method will allow the users to edit the organisation and
  def promote
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])

    # Get the user
    user_id = params.dig(:user_id)
    if user_id.present?
      # Get the user
      @user = @organisation.organisation_users.find_by(user_id: user_id)

      # Check that the user has been found
      if @user.present?
        # Turn the edit on the association
        @user.update(can_edit: true)
        # Return the json for the organisation
        render json: { organisation: format_organisation }
      else
        # Send the 404 message
        render json: { error: 'Failed to find the user' }, status: 404
      end
    else
      # Return the error message
      render json: { errors: { user_id: [ 'cannot be blank' ] } }, status: 422
    end
  end

  # This method will stop users from making changes
  def demote
    # Get the orgaisation that has been requested
    @organisation = current_user.organisations.find(params[:organisation_id])

    # Get the user
    user_id = params.dig(:user_id)
    if user_id.present?
      # Get the user
      @user = @organisation.organisation_users.find_by(user_id: user_id)

      # Check that the user has been found
      if @user.present?
        # Turn the edit off of the association
        @user.update(can_edit: false)
        # Return the json for the organisation
        render json: { organisation: format_organisation }
      else
        # Send the 404 message
        render json: { error: 'Failed to find the user' }, status: 404
      end
    else
      # Return the error message
      render json: { errors: { user_id: [ 'cannot be blank' ] } }, status: 422
    end
  end

  private

  # This method will be used to structure the orgaisation value and how it is returned
  def format_organisation
    {
      id: @organisation.id,
      name: @organisation.name,
      description: @organisation.description,
      snippet: @organisation.snippet,
      sign_up_mode: @organisation.sign_up_mode,
      sign_up_code: @organisation.sign_up_code,
      user_can_edit: current_user.can_edit_organisation?(@organisation),
      users: organisation_users,
      owner: @organisation.owner,
      courses: @organisation.courses.map{ |course| course.for_json(current_user) }
    }
  end

  def organisation_users
    @organisation.organisation_users.map{ |user|
      {
        can_edit: user.can_edit,
        id: user.user_id,
        first_name: user.user.first_name,
        last_name: user.user.last_name,
        email: user.user.email
      }
    }
  end

  # This method will be used to get the record for the requested object
  def set_organisation
    # Find the organisation by the ones that they are assigned to
    @organisation = current_user.organisations.find(params[:id])
  end

  # This method will return the data that is submitted so it can be used to create and update the
  # record
  def organisation_params
    params.require(:organisation).permit(:name, :description, :sign_up_mode, :sign_up_code)
  end

  # This method will define the params that can be used to create a user
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name)
  end
end
