class Presentations::BlocksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lesson
  before_action :set_block, only: [:show]

  def show
    render json: {
      lesson: @lesson.for_present_json,
      section: @block.section,
      block: @block.for_show_json,
      next: @block.next_in_presentation,
      prev: @block.previous_in_presentation
    }
  end

  private

  def set_lesson
    # Try and get the lesson they are trying to present
    lessons = Lesson.joins(:course).where(lessons: { id: params[:lesson_id] }, courses: { id: current_user.courses.pluck(:id) })
    # Can a lesson be found
    if lessons.any?
      @lesson = lessons.first
    else
      # returnt a 404 error
      return render json: { error: "Could not find the requested lesson" }, status: 404
    end
  end

  def set_block
    # Try and find the block needed
    blocks = @lesson.blocks.where(id: params[:id])
    # Check that a block can be found
    if blocks.any?
      @block = blocks.first
    else
      # returnt a 404 error
      return render json: { error: "Could not find the requested block" }, status: 404
    end
  end
end
