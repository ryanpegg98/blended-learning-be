class Presentations::LessonsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_lesson, only: [:show]

  def show
    # Render the JSON for the lesson
    render json: { lesson: @lesson.for_present_json, first_slide: @lesson.first_block, last_slide: @lesson.last_block }
  end

  private

  def set_lesson
    # Try and get the lesson they are trying to present
    lessons = Lesson.joins(:course).where(lessons: { id: params[:id] }, courses: { id: current_user.courses.pluck(:id) })
    # Can a lesson be found
    if lessons.any?
      @lesson = lessons.first
    else
      # returnt a 404 error
      return render json: { error: "Could not find the requested lesson" }, status: 404
    end
  end
end
