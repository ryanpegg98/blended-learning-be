class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
    # Show the current users details, without passing the password back
    render json: { user: current_user.for_json }
  end

  # This method will allow the user to update their details
  def update
    # Assign the personal info to the current user but do not save
    @user = current_user
    @user.assign_attributes(user_details_params)
    # Check that a password has been added before assigning the password
    password = params.dig(:user, :password)
    password_confirmation = params.dig(:user, :password_confirmation)
    if password.present?
      # Assign the password values
      @user.password = password
      # If the password confirmation is not present put a space in to force the validation
      password_confirmation = ' ' unless password_confirmation.present?
      @user.password_confirmation = password_confirmation
    end

    # Attempt to save the user
    if @user.save
      # Return the user without security infomation
      render json: { user: @user.for_json }
    else
      # If it fails return the reason
      render json: { errors: @user.errors }, status: 422
    end
  end

  # This method will be used to get the courses they have
  def my_courses
    # Get the last four courses that the user joined as their most recent
    render json: { courses: current_user.recent_courses(4) }
  end

  private

  def user_details_params
    # This will only require the information that is not the password
    params.require(:user).permit(:first_name, :last_name, :email)
  end

  def password_params
    # This will get the password_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
