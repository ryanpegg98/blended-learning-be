class BlockTypesController < ApplicationController
  before_action :authenticate_user!

  def index
    @block_types = BlockType.all

    render json: { block_types: @block_types.map{ |type| type.for_json } }
  end

  def show
    @block_type = BlockType.find(params[:id])

    if @block_type.present?
      render json: { block: @block_type.for_json }
    else
      render json: { error: 'Unable to find block type' }, status: 404
    end
  end
end
