class SessionsController < Devise::SessionsController
  respond_to :json

  private

  def respond_with(resource, _opts = {})
    render json: { user: resource, token: current_token }
  end

  def respond_to_on_destroy
    head :no_content
  end

  def current_token
    # get the current token that has been created for this user
    request.env['warden-jwt_auth.token']
  end
end