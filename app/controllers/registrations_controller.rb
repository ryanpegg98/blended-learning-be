class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
    build_resource(sign_up_params)
    resource.user_type = UserType.learner

    if resource.save
      sign_in(resource_name, resource)
      render json: { user: resource, token: current_token }
    else
      render json: { errors: resource.errors }, status: 422
    end
  end

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  private

  def current_token
    request.env['warden-jwt_auth.token']
  end
end