class Learners::SectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course
  before_action :set_lesson
  before_action :set_section, only: [:show]

  def show

    render json: {
      course: @course.for_json(current_user),
      lesson: @lesson.for_json,
      section: @section.for_json,
      previous_section: @section.previous_section,
      next_section: @section.next_section
    }
  end

  private

  # Set the course
  def set_course
    # Attempt to get the course for the user
    @course = current_user.courses.find(params[:course_id])
    # If the course can not be found send the 404 message
    unless @course.present?
      return render json: { error: 'Failed to find the course' }, status: 404
    end
  end

  def set_lesson
    # Attempt to get the lesson that belongs to the course
    @lesson = @course.lessons.find(params[:lesson_id])
    # If the lesson cannot be found send the 404 message
    unless @lesson.present?
      return render json: { error: 'Failed to find the lesson' }, status: 404
    end
  end

  # Get the section that has been requested
  def set_section
    # Attempt to get the section that is associated with the lesson
    @section = @lesson.sections.find(params[:id])
    # Check that that the section has been selected
    unless @section.present?
      return render json: { error: 'Failed to find the section' }, status: 404
    end
  end
end
