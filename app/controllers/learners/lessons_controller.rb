class Learners::LessonsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course
  before_action :set_lesson, only: [:show]

  def show
    # Render the json for the course and the lesson
    render json: { course: @course.for_json(current_user), lesson: @lesson.for_json }
  end

  private

  # Get the course that has been requested
  def set_course
    # Attempt to get the courses that have been assigned to the user
    @course = current_user.courses.find(params[:course_id])
    # If the course could not be found return a 404 message
    unless @course.present?
      return render json: { error: "Failed to find the requested course" }, status: 404
    end
  end

  def set_lesson
    # Attempt to get the lesson for the course as requested
    @lesson = @course.lessons.find(params[:id])
    # If the lesson could not befound render a 404 message
    unless @lesson.present?
      return render json: { error: 'Failed to find the requested course' }, status: 404
    end
  end
end
