class Learners::CoursesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course, only: [:show]
  before_action :course_via_code, only: [:course_information, :join_course]

  def index
    # This will show all of the courses that the user is a part of as a teacher
    # or a learner
    render json: { courses: current_user.courses.map{ |course| course.for_json(current_user) } }
  end

  def show
    # Show the course as json for the current user
    render json: { course: @course.for_json(current_user) }
  end

  def course_information
    # Render the course as JSON
    render json: { course: @course.for_json(current_user) }
  end

  def join_course
    # Attempt to add the user to the course
    course_user = @course.learner_users.find_or_create_by(user_id: current_user.id)

    # Render the course as JSON
    render json: { course: @course.for_json(current_user) }
  end

  private

  # This method will get the course via the params
  def set_course
    # Attempt to find the course in the users courses
    @course = current_user.courses.find(params[:id])

    # Return the missing message
    unless @course.present?
      return render json: { error: 'Could not find the requested course' }, status: 404
    end
  end

  # This method will get a course using a code
  def course_via_code
    # get the code that was sent
    code = params.dig(:course, :code)
    # If the code cannot be vound render the error message
    unless code.present?
      return render json: { errors: { code: [ 'cannot be blank' ] } }, status: 422
    end
    # Get the course that code belongs to from the orgaisation the user has access to
    courses = Course.joins(organisation: :organisation_users)
      .where(
        organisation_users: { user_id: current_user.id },
        courses: { sign_up_code: code, sign_up_mode: 'code_only' }
      )

    # check that there are courses
    if courses.any?
      # Return the first course
      @course = courses.first
    else
      # Render the error messages as json
      return render json: { errors: { code: [ 'is not beign used by any courses' ] } }, status: 422
    end
  end

end
