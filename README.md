# Blended Learning API

This project was built as a part of a project to develop an application which can help to deliver blended learning.

This project requires Ruby version 2.7.1 to be installed. This can be done by installing RVM which will allow you to install multiple Ruby versions which you can the use.

* [Install RVM](https://rvm.io/rvm/install)

* Once RVM is installed you will need to install MySQL and create an account so that you can create the database.

* When MySQL is installed change the `database.yml` file to go to the local host and change the user details.

* Run the command `rails db:create` that will create the database

* Once created you can either import the database supplied or migrate the database by running the command `rails db:migrate`

* Then run the command to run the data migrations using the following command `rails data:migrate`

* When you are ready to start the application it is important to run it on port `3001` as otherwise it will interfere with the Frontend application this can be done by defining the port number when starting the application. `rails s -p 3001`

## Using the application

If you have imported the database that is label `backend_database.sql` which can be found root directory of the compressed files.

It is recommended that the database is imported as it has all of the data ready to use with an organisation and a course.

The organisation code of `edge_hill` to sign up to the organisation.

The password for the Ruby on Rails course is `ruby_on_rails` which can be accessed by going to the organisation
