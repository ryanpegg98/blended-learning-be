FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'password' }
    password_confirmation { password }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }

    factory :user_super_admin do 
      user_type { UserType.super_admin }
    end

    factory :user_admin do 
      user_type { UserType.admin }
    end

    factory :user_teacher do 
      user_type { UserType.teacher }
    end

    factory :user_learner do 
      user_type { UserType.learner }
    end
  end
end
