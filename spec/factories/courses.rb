FactoryBot.define do
  factory :course do
    name { "MyString" }
    organisation { nil }
    owner { nil }
    description { "MyText" }
    active { false }
  end
end
