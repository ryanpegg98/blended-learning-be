FactoryBot.define do
  factory :organisation_user do
    organisation { create(:organisation) }
    user { create(:user_admin) }
    can_edit { false }
  end
end
