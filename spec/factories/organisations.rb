FactoryBot.define do
  factory :organisation do
    name { "MyString" }
    owner { create(:user_admin) }
    description { "MyText" }
    sign_up_mode { 1 }
    sign_up_code { "MyString" }
  end
end
