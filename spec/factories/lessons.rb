FactoryBot.define do
  factory :lesson do
    course { nil }
    title { "MyString" }
    description { "MyText" }
    rank { 1 }
  end
end
