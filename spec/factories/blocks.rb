FactoryBot.define do
  factory :block do
    section { nil }
    block_type { nil }
    show_in_slides { false }
    rank { 1 }
  end
end
