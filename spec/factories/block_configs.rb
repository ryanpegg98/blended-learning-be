FactoryBot.define do
  factory :block_config do
    block { nil }
    config_type { "" }
    value { "MyText" }
  end
end
