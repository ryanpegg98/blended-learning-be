FactoryBot.define do
  factory :block_type do
    name { "MyString" }
    system_name { "MyString" }
  end
end
