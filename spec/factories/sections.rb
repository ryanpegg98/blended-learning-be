FactoryBot.define do
  factory :section do
    lesson { nil }
    title { "MyString" }
    rank { 1 }
  end
end
