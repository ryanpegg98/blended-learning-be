FactoryBot.define do
  factory :course_user do
    course { nil }
    user { nil }
    can_edit { false }
  end
end
