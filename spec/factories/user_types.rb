FactoryBot.define do
  factory :user_type do
    name { Faker::Job.title }
  end
end
