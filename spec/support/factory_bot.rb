# Configuration to allow Factory bot to work with RSpec
RSpec.configure do |config|
  # Add the methods to the config
  config.include FactoryBot::Syntax::Methods
end