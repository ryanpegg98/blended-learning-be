require 'rails_helper'

RSpec.describe Lesson, type: :model do
  # Validation specs
  describe "Validations" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:course) }
    it { should validate_presence_of(:rank) }
  end

  # Associations
  describe "Associations" do
    it { should belong_to(:course) }
    it { should have_many(:sections) }
  end
end
