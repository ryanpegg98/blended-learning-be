require 'rails_helper'

RSpec.describe Organisation, type: :model do
  # Check that all the validations have been added
  describe "Validations" do
    subject { build(:organisation) }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_presence_of(:owner_id) }
    it { should validate_presence_of(:sign_up_mode) }
  end

  # Check the associations
  describe "Associations" do
    it { should belong_to(:owner) }
    it { should have_many(:users) }
    it { should have_many(:courses) }
  end
end
