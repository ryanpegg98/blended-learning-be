require 'rails_helper'

RSpec.describe BlockType, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:system_name) }
  end
end
