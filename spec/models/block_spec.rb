require 'rails_helper'

RSpec.describe Block, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:section) }
    it { should validate_presence_of(:block_type) }
    it { should validate_presence_of(:rank) }
  end

  # Associations
  describe "Associations" do
    it { should belong_to(:section) }
    it { should belong_to(:block_type) }
    it { should have_many(:block_configs) }
  end
end
