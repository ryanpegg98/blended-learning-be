require 'rails_helper'

RSpec.describe OrganisationUser, type: :model do
  # Check for validations
  describe "Validations" do
    subject { build(:organisation_user) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:organisation_id) }
    it { should validate_inclusion_of(:can_edit).in_array([true, false]) }
    it { should validate_uniqueness_of(:user_id).scoped_to(:organisation_id) }
  end

  # Check that all the associations are working
  describe "Associations" do
    it { should belong_to(:user) }
    it { should belong_to(:organisation) }
  end
end
