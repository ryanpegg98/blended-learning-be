require 'rails_helper'

RSpec.describe CourseUser, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:course) }
    it { should validate_presence_of(:user) }
    it { should validate_inclusion_of(:can_edit).in_array([true, false]) }
  end

  # Associations
  describe "Associations" do
    it { should belong_to(:user) }
    it { should belong_to(:course) }
  end
end
