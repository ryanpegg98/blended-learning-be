require 'rails_helper'

RSpec.describe User, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end

  # Test for the associations it should have
  describe "Associations" do
    it { should belong_to(:user_type) }
    it { should have_many(:organisations) }
    it { should have_many(:courses) }
  end

  # Check for the method to return the users full name
  describe "Do I see the users full name" do
    it 'should return the users first name and last name joined' do
      user = create(:user_super_admin)

      # Get the full name value from the model
      full_name = user.full_name

      # Check that the value is correct
      expect(full_name).to satisfy { |value| value == "#{user.first_name} #{user.last_name}" }
    end
  end
end
