require 'rails_helper'

RSpec.describe Section, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:lesson) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:rank) }
  end

  # Associations
  describe "Associations" do
    it { should belong_to(:lesson) }
    it { should have_many(:blocks) }
  end
end
