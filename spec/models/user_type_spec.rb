require 'rails_helper'

RSpec.describe UserType, type: :model do
  # Test for the validations for the user type
  describe "Validations" do
    subject { build(:user_type) }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end

  # Test that the correct associations have been added
  describe "Assocications" do
    it { should have_many(:users) }
  end
end
