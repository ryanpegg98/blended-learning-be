require 'rails_helper'

RSpec.describe BlockConfig, type: :model do
  # Validations
  describe "Validations" do
    it { should validate_presence_of(:block) }
    it { should validate_presence_of(:config_name) }
  end

  # Associations
  describe "Associations" do
    it { should belong_to(:block) }
    it { should have_one(:block_type) }
  end
end
