require 'rails_helper'

RSpec.describe Course, type: :model do
  # Specs to check for validation
  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:owner) }
    it { should validate_presence_of(:organisation) }
    it { should validate_inclusion_of(:active).in_array([true, false]) }
  end

  # Specs to check for the associations
  describe "Associations" do
    it { should belong_to(:organisation) }
    it { should belong_to(:owner) }
    it { should have_many(:users) }
    it { should have_many(:teachers) }
    it { should have_many(:learners) }
    it { should have_many(:lessons) }
  end
end
