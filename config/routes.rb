Rails.application.routes.draw do
  # All routes should go inside of the api and the version number
  scope :api do
    scope :v1 do
      # Set the devise routes
      devise_for :users,
                  path: '',
                  path_names: {
                    sign_in: 'login',
                    sign_out: 'logout',
                    registration: 'signup'
                  },
                  controllers: {
                    sessions: 'sessions',
                    registrations: 'registrations'
                  }

      # This wroute will handle the request for the current user details
      resource :user, only: [:show, :update] do
        # Add the route for the path to get the users courses
        get :my_courses
      end
      namespace :organisations do
        # Add the controller for the new organisation
        resource :new_organisation, only: [:create]
        # This route will handle the requests for adding, updateing and viewing the organisations
        resources :organisations, only: [:index, :create, :show, :update, :destroy] do
          # Add the routes that will allow requests to link and unlink users to and from the organisations
          patch :link
          patch :unlink
          # Add the routes to pronte and demote users
          patch :promote
          patch :demote

          # Add the routes to allow organisations to create teachers
          post :create_teacher
          # Add a route to allow learners to be created
          post :create_learner

          # Add the resources for the courses
          resources :courses, only: [:index, :create, :show, :update, :destroy] do
            # Add the route to get the users that can be added to a course
            get 'available_users'
            # Add the route to allow a user to join a course
            patch 'join_course'
            # Add the routes to add learners and teachers
            patch 'add_teacher'
            patch 'add_learner'
            # Add the routes to pronte and demote users
            patch 'promote'
            patch 'demote'

            # Add the resources for lessons
            resources :lessons, only: [:index, :create, :show, :update, :destroy] do
              # Added the actions to move lessons up and down
              patch 'move_up'
              patch 'move_down'

              # Add the resources for sections
              resources :sections, only: [:create, :show, :update, :destroy] do
                # Added the actions to move sections up and down
                patch 'move_up'
                patch 'move_down'

                # Add the controller to create, edit and destroy the blocks in a section
                resources :blocks, only: [:create, :show, :update, :destroy] do
                  # Added the actions to move blocks up and down
                  patch 'move_up'
                  patch 'move_down'
                end
              end
            end
          end
        end

        # Route to check the joining code
        post 'organisations/joining_code', to: 'organisations#joining_code'
        # Route to allow users to join an organisation via a code
        post 'organisations/join_organisation', to: 'organisations#join_organisation'
      end

      # Create the namespace for the presentations routes
      namespace :presentations do
        # Add the lessons controller
        resources :lessons, only: [:show] do
          # Add the route to get a block for the lesson
          resources :blocks, only: [:show]
        end
      end

      # Add the namespace for the learners controllers
      namespace :learners do
        # Add the route to get the course info via a code
        post 'courses/course_information', to: 'courses#course_information'
        # Add the route to allow learners to join a course
        post 'courses/join_course', to: 'courses#join_course'

        # Add the controller for courses
        resources :courses, only: [:index, :show] do
          # Nest the lessons controller in the courses controller
          resources :lessons, only: [:show] do
            # Nest the sections controller within the lessons controller
            resources :sections, only: [:show]
          end
        end
      end

      # Add the routes for the block types
      resources :block_types, only: [:index, :show]
    end
  end
end
