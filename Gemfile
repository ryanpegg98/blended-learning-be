source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2', '>= 6.0.2.2'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.4.4'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Add the testing framework RSpec
  gem 'rspec-rails', '~> 4.0'
  # Add the factory bot to be used with RSpec
  gem 'factory_bot_rails', '~> 5.1', '>= 5.1.1'
  # Add the gem that will be used for intergration testing
  gem 'capybara', '~> 3.32', '>= 3.32.1'
end

group :test do
   # Add the gem to handle the data cleaning of the database after testing
   gem 'database_cleaner', '~> 1.8', '>= 1.8.4'
   # Add shoulda matches to check for validation
   gem 'shoulda-matchers', '~> 4.3'
   # Add a gem that will create fake values for the tests
   gem 'faker', '~> 2.11'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # Add guard to use while developing the api this will run the tests while
  # working on the project
  gem 'guard-rspec', '~> 4.7', '>= 4.7.3'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# These gems have been added to aid the development of the project

# Authentication will be handled using Devise and JWT Tokens
gem 'devise', '~> 4.7', '>= 4.7.1'
gem 'devise-jwt', '~> 0.6.0'
# Figaro makes it easier to set environment variables
gem "figaro"

# Adding a gem that will make it easier to add data to the data base
gem 'data_migrate', '~> 6.3'

# Adding capistrano to handle the deploy
group :development do
  gem "capistrano", "~> 3.10", require: false
  gem "capistrano-rails", "~> 1.4", require: false
  gem 'capistrano-rvm'
  gem 'capistrano-passenger'
end

# Add the gem that will convert markdown into plain text
gem 'redcarpet', '~> 3.5'
