class CreateBlockTypes < ActiveRecord::Migration[6.0]
  BLOCK_TYPES = [
    { name: 'Banner', system_name: 'banner' },
    { name: 'Image Block', system_name: 'image_block' },
    { name: 'Video Block', system_name: 'video_block' },
    { name: 'Image Text Block', system_name: 'image_text_block' },
    { name: 'Link Block', system_name: 'link_block' },
    { name: 'Text Block', system_name: 'text_block' },
    { name: 'Quote Block', system_name: 'quote_block' }
  ]

  def up
    BLOCK_TYPES.each do |block|
      BlockType.find_or_create_by(name: block[:name], system_name: block[:system_name])
    end
  end

  def down
    BLOCK_TYPES.each do |block|
      blocks = BlockType.where(name: block[:name], system_name: block[:system_name])
      blocks.destroy_all
    end
  end
end
