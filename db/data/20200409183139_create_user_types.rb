class CreateUserTypes < ActiveRecord::Migration[6.0]
  def up
    UserType.find_or_create_by(name: 'Super Admin')
    UserType.find_or_create_by(name: 'Admin')
    UserType.find_or_create_by(name: 'Teacher')
    UserType.find_or_create_by(name: 'Learner')
  end

  def down
    UserType.where(name: ['Super Admin', 'Admin', 'Teacher', 'Learner']).destroy_all
  end
end
