class CreateOrganisations < ActiveRecord::Migration[6.0]
  def change
    create_table :organisations do |t|
      t.string :name, null: false, unique: true
      t.references :owner, null: false, foreign_key: { to_table: :users }, index: true
      t.text :description, null: true
      t.integer :sign_up_mode, null: false, default: 0
      t.string :sign_up_code, null: true

      t.timestamps
    end
  end
end
