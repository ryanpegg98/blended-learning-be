class CreateLessons < ActiveRecord::Migration[6.0]
  def change
    create_table :lessons do |t|
      t.references :course, null: false, foreign_key: { to_table: :courses }, index: true
      t.string :title, null: false
      t.text :description, null: true
      t.integer :rank, null: false, default: 0

      t.timestamps
    end
  end
end
