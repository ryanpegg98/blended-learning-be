class CreateBlockTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :block_types do |t|
      t.string :name, null: false
      t.string :system_name, null: false

      t.timestamps
    end
  end
end
