class CorrectColumnForUserOnOrganisationUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :organisation_users, :user_id_id
    add_reference :organisation_users, :user, null: false, foreign_key: { to_table: :users }, index: true
  end
end
