class AddExpToJwtBlacklist < ActiveRecord::Migration[6.0]
  def change
    add_column :jwt_blacklist, :exp, :string, null: false
  end
end
