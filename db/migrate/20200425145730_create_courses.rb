class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.references :organisation, null: false, foreign_key: { to_table: :organisations }, index: true
      t.references :owner, null: false, foreign_key: { to_table: :users }, index: true
      t.text :description
      t.boolean :active, null: false, default: false

      t.timestamps
    end
  end
end
