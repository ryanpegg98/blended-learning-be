class AddUserTypeIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :user_type, null: true, foreign_key: { to_table: :user_types }, index: true
  end
end
