class AddCourseJoiningToCourses < ActiveRecord::Migration[6.0]
  def change
    add_column :courses, :sign_up_mode, :integer, null: false, default: 0
    add_column :courses, :sign_up_code, :string, null: true
  end
end
