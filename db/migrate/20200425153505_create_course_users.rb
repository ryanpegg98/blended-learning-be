class CreateCourseUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :course_users do |t|
      t.references :course, null: false, foreign_key: { to_table: :courses }, index: true
      t.references :user, null: false, foreign_key: { to_table: :users }, index: true
      t.boolean :can_edit, null: false, default: false

      t.timestamps
    end
  end
end
