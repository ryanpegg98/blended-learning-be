class CreateBlockConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :block_configs do |t|
      t.references :block, null: false, foreign_key: { to_table: :blocks }, index: true
      t.integer :config_type, null: false, default: 0
      t.text :value, null: true

      t.timestamps
    end
  end
end
