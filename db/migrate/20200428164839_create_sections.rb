class CreateSections < ActiveRecord::Migration[6.0]
  def change
    create_table :sections do |t|
      t.references :lesson, null: false, foreign_key: { to_table: :lessons }, index: true
      t.string :title, null: false
      t.integer :rank, null: false, default: 0

      t.timestamps
    end
  end
end
