class ChangeConfigTypeColumnOnBlockConfigs < ActiveRecord::Migration[6.0]
  def change
    remove_column :block_configs, :config_type
    add_column :block_configs, :config_name, :string, null: false
  end
end
