class CreateOrganisationUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :organisation_users do |t|
      t.references :organisation, null: false, foreign_key: { to_table: :organisations }, index: true
      t.references :user_id, null: false, foreign_key: { to_table: :users }, index: true
      t.boolean :can_edit, null: false, default: false

      t.timestamps
    end
  end
end
