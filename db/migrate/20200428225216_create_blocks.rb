class CreateBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :blocks do |t|
      t.references :section, null: false, foreign_key: { to_table: :sections }, index: true
      t.references :block_type, null: false, foreign_key: { to_table: :block_types }, index: true
      t.boolean :show_in_slides, null: false, default: true
      t.integer :rank, null: false, default: 0

      t.timestamps
    end
  end
end
